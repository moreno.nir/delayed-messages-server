# Delayed messages server (Node.js and Redis)

a simple application server that prints a message at a given time in the future.

The server has only 1 API:

echoAtTime - which receives two parameters, time and message, and writes that message to the server console at the given time.

The server resistent to restarts and use redis to persist the messages and the time they should be sent at

## Getting Started
Make sure you have a running instance of redis on your local machine
Download any application to interact with HTTP APIs (Postman, Insomnia, etc..)

Clone & Build the project

Run the Server (npm run start)

Send an http post request to http://localhost:3000/echoAtTime (with body: message and time)

Server will display the message on the given time.

### Prerequisites
Install Redis for Node.js (https://redis.io/download)

Run Redis (with a default port: 6379)

### Installing
```
npm install
npm run start
```
send post request to: http://localhost:3000/echoAtTime

body:
```
{
	"message" : "message content...",
	"time": "2020-01-20T20:00:00.000Z"
}
```