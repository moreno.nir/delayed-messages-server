import MessagesProducer from '../Dal/MessagesProducer';
import Validator from "../Utils/Validator";

class MessagesRouter {
    constructor(setName){
        this.messagesProducer = new MessagesProducer(setName);
    }

    getRouter(router) {
        router.post('/echoAtTime', (req, res) => {
            const time = req.body.time;
            const message = req.body.message;


            if(!Validator.isString(message)){
                res.status(400);
                res.send("message parameter should be typeof string");
                return;
            }

            if(!Validator.isUnixDate(time)){
                res.status(400);
                res.send("time parameter should be in ISO format");
                return;
            }

            this.messagesProducer.sendMessage(message, time);
            res.send('ok. message will be delivered');
        });

        return router;
    };
}

export default MessagesRouter;