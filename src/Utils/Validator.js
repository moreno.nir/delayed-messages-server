import moment from "moment";

class Validator {
    static isUnixDate(date) {
        return moment(date, 'YYYY-MM-DDTHH:mm:ss.SSS[Z]', true).isValid();
    }

    static isString(str) {
        return typeof str === "string";
    }
}

export default Validator;
