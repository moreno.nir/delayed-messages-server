import express from 'express';
import bodyParser from 'body-parser';
import MessagesRouter from './Routes/MessagesRouter';
import MessagesConsumer from "./Dal/MessagesConsumer";

const SET_NAME = 'messagesSortedSet';

export default class App {
    async start(port) {
        /*
        * Consumer to read messages and print.
        * */
        const consumer = new MessagesConsumer(SET_NAME);
        consumer.consume();


        const app = express();
        /*
        * Express app which expose 1 API:
        * POST /echoAtTime
        * BODY:
        * {
        *   "time": "2020-01-20T20:47:58.086Z"  -- UTC ISO
        *   "message": "message content ..."
        * }
        * */
        const messagesRouter = new MessagesRouter(SET_NAME);
        app.use(bodyParser.json());
        app.use(messagesRouter.getRouter(express.Router()));
        return app.listen(port);
    }
}