import App from './app';
import MessagesConsumer from "./Dal/MessagesConsumer";

const PORT = 3000;
const TIMEOUT = 1000;

/*
* Start Server
* */
const app = new App();
app.start(PORT).then(() => {
    console.log(`App is running at http://localhost:${PORT}`);
    console.log(`Press CTRL-C to stop\n`);
});