import Redis from "ioredis";
import moment from "moment";

class MessagesConsumer {
    constructor(setName) {
        this.redis = new Redis();
        this.setName = setName;
        this.printMessagesQueueLength();
    }

    static print(message) {
        const msg = JSON.parse(message);
        console.log(`Message: ${msg.message} (${msg.time})`);
    }

    consume(interval = 1000) {
        return setInterval(() => {
            const now = moment().unix();
            this.getMessagesByScore(now);
        }, interval);
    }

    getMessagesByScore(score) {
        this.redis.zrangebyscore(this.setName, 0, score, (err, result) => {
            if (result) {
                result.forEach((message) => {
                    MessagesConsumer.print(message);
                });
                this.removeMessagesByScore(score);
            }
        });
    }

    removeMessagesByScore(score) {
        this.redis.zremrangebyscore(this.setName, 0, score, (err, result) => {
            if (result) {
                this.printMessagesQueueLength();
            }
        });
    }

    printMessagesQueueLength() {
        this.redis.zcard(this.setName, (err, result) => {
            if (result) {
                console.log(`* ${result} messages in queue *`);
            } else {
                console.log('* Message queue is empty *');
            }
        });
    }
}

export default MessagesConsumer;