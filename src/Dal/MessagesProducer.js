import Redis from 'ioredis';
import moment from 'moment';

class MessagesProducer {
    constructor(setName) {
        this.redis = new Redis();
        this.setName = setName;
    }

    sendMessage(message, time) {
        const timeToApply = moment(time).unix();
        this.redis.zadd(this.setName, timeToApply, JSON.stringify({
            message,
            time
        }));
    }
}

export default MessagesProducer;